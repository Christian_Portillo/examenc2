package com.example.examenc2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class IngresoActivity extends AppCompatActivity {

    private TextView txtNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);

        txtNombre = (TextView) findViewById(R.id.txtNombre);

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        txtNombre.setText(nombre);

    }
}
